package com.example.komikid.Episode;

public class AdapterEK {
    private String id_ek;
    private String ket_ek;
    private String jdl_ek;

    AdapterEK(String id_ek, String jdl_ek, String ket_ek) {
        this.id_ek = id_ek;
        this.jdl_ek = jdl_ek;
        this.ket_ek = ket_ek;
    }

    public String getId_ek() {
        return id_ek;
    }

    public String getKet_ek() {
        return ket_ek;
    }

    public String getJdl_ek() {
        return jdl_ek;
    }
}
