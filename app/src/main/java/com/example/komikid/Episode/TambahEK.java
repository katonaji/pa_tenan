package com.example.komikid.Episode;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.komikid.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Objects;

import pub.devrel.easypermissions.EasyPermissions;

public class TambahEK extends AppCompatActivity {

    public static final int RESULT_LOAD_IMAGE = 1;
    public static final int TAKE_PICTURE = 2;

    String id, id_komik;
    ImageView imageView;
    EditText edjdlKomik,edket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_ek);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        imageView = findViewById(R.id.imgvEK);
        edjdlKomik = findViewById(R.id.edjdlEK);
        edket = findViewById(R.id.coba2);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        id_komik = bundle.getString("id_komik");
        id = bundle.getString("id_episode");

        Button bpfg = findViewById(R.id.btn1PicFromGalery);
        bpfg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE};
                if (EasyPermissions.hasPermissions(getApplicationContext(), galleryPermissions)){
                    pickImageFromGallery();
                } else {
                    EasyPermissions.requestPermissions(TambahEK.this, "Access for storage",
                            101, galleryPermissions);
                    pickImageFromGallery();
                }
            }
        });
    }

    void pickImageFromGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filepathcolumn = { MediaStore.Images.Media.DATA };

            assert selectedImage != null;
            Cursor cursor = getContentResolver().query(selectedImage,
                    filepathcolumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filepathcolumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = new BitmapDrawable(getApplicationContext().getResources(),
                    BitmapFactory.decodeFile(picturePath)).getBitmap();
            int nh = (int) ( bitmap.getHeight() * (1024.0 / bitmap.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 1024, nh, true);
            imageView.setImageBitmap(scaled);
        }

        if(requestCode == TAKE_PICTURE && resultCode == RESULT_OK) {
            //Bitmap bitmap1 = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
            File sdCard = Environment.getExternalStorageDirectory();
            File imgFile = new File(sdCard.getAbsolutePath() + "/DCIM/temp.jpg");
            Bitmap bitmap1 = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            Bitmap bitmap = new BitmapDrawable(getApplicationContext().getResources(),
                    bitmap1).getBitmap();
            int nh = (int) ( bitmap.getHeight() * (1024.0 / bitmap.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 1024, nh, true);
            imageView.setImageBitmap(scaled);
        }
    }

    public void tambahDataEK(View view) {

        DBHelper dbHelper = new DBHelper(this);
        ContentValues values = new ContentValues();

        if(TextUtils.isEmpty(edjdlKomik.getText().toString().trim())) {
            if(TextUtils.isEmpty(edjdlKomik.getText().toString().trim())) {
                Snackbar.make(view, "Judul komik dan Deskripsi harus diisi", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
            else{
                Snackbar.make(view, "Judul harus diisi", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        } else if(TextUtils.isEmpty(edket.getText().toString().trim())){
            Snackbar.make(view, "Keterangan harus diisi", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else {
            values.put(DBHelper.id_ekomik, id);
            values.put(DBHelper.jdl_ek, edjdlKomik.getText().toString());
            values.put(DBHelper.ket_ek, edket.getText().toString());
            dbHelper.InsertEK(values);

            Snackbar.make(view, "Berhasil Tambah Data", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE};

            if (EasyPermissions.hasPermissions(getApplicationContext(), galleryPermissions)) {
                BitmapDrawable draw = (BitmapDrawable) imageView.getDrawable();

                if(draw != null) {
                    Bitmap bitmap = draw.getBitmap();
                    FileOutputStream outStream = null;
                    File sdCard = Environment.getExternalStorageDirectory();
                    File dir = new File(sdCard.getAbsolutePath() + "/komik_id/img");
                    boolean isDirectoryCreated = dir.exists();
                    if (!isDirectoryCreated) {
                        isDirectoryCreated = dir.mkdirs();
                    }
                    if(isDirectoryCreated) {
                        String sjdl = edjdlKomik.getText().toString();
                        String sket = edket.getText().toString();
                        String fileName = String.format(Locale.getDefault(),
                                sjdl + " " + sket + ".jpg", System.currentTimeMillis());
                        File outFile = new File(dir, fileName);
                        try {
                            outStream = new FileOutputStream(outFile);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                        try {
                            Objects.requireNonNull(outStream).flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            Objects.requireNonNull(outStream).close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

            } else {
                EasyPermissions.requestPermissions((Activity) getApplicationContext(), "Access for storage",
                        101, galleryPermissions);
            }
        }

    }

    public void clearDataEK(View view) {
        imageView.setImageDrawable(null);
        edjdlKomik.setText(null);
        edket.setText(null);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), TampilEK.class);
        Bundle bundle = new Bundle();
        bundle.putString("id_komik", id_komik);
        bundle.putString("id_episode", id);

        intent.putExtras(bundle);
        startActivity(intent);
        TambahEK.this.finish();
    }
}
