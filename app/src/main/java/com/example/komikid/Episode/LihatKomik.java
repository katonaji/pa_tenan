package com.example.komikid.Episode;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.komikid.R;

import java.io.File;
import java.util.ArrayList;

public class LihatKomik extends AppCompatActivity implements SearchView.OnQueryTextListener{

    DBHelper dbHelper;
    boolean b;
    SearchView searchView;
    String publ;

    int jml = 0;
    TextView juml;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_komik);

        Toolbar toolbar = findViewById(R.id.toolbar_lht);
        setSupportActionBar(toolbar);

        //juml = findViewById(R.id.jmlh);

        snackbarHapus();

        dbHelper = new DBHelper(this);
        loadTable();

    }

    ArrayList<AdapterItems> listnewsData = new ArrayList<>();
    MyCustomAdapter myadapter;

    private void loadTable() {
        listnewsData.clear();
        String[] projection = {DBHelper.id, DBHelper.jdl_kmk, DBHelper.ket, DBHelper.genre, DBHelper.publish, DBHelper.author};
        String selection = "publish=1";
        String sortorder = DBHelper.id + " DESC";
        Cursor cursor = dbHelper.Query(projection, selection, null, sortorder);
        if (cursor.moveToFirst()) {
            do {
                listnewsData.add(new AdapterItems(cursor.getString(cursor.getColumnIndex(DBHelper.id)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.jdl_kmk)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.ket)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.genre)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.publish)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.author))
                ));
            } while (cursor.moveToNext());
        }

        myadapter = new MyCustomAdapter(listnewsData);
        ListView lsNews = findViewById(R.id.lvEpsTampil);
        lsNews.setAdapter(myadapter);
        TextView tvKosong = findViewById(R.id.tvEpsKosong);

        if(myadapter.isEmpty()) {
            lsNews.setVisibility(View.GONE);
            tvKosong.setVisibility(View.VISIBLE);
        } else {
            lsNews.setVisibility(View.VISIBLE);
            tvKosong.setVisibility(View.GONE);
        }
    }

    private class MyCustomAdapter extends BaseAdapter {
        ArrayList<AdapterItems> listnewsDataAdpater ;

        MyCustomAdapter(ArrayList<AdapterItems> listnewsDataAdpater) {
            this.listnewsDataAdpater=listnewsDataAdpater;
        }

        @Override
        public int getCount() {
            return listnewsDataAdpater.size();
        }

        @Override
        public String getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater mInflater = getLayoutInflater();
            View myView = mInflater.inflate(R.layout.layout_tiket,null);
            final AdapterItems s = listnewsDataAdpater.get(position);

            final TextView tvJdlKmk = myView.findViewById(R.id.tvmerk);
            tvJdlKmk.setText(s.getJdl_kmk());

            final TextView tvGenre = myView.findViewById(R.id.tvgenre);
            tvGenre.setText(s.getGenre());

            final TextView tvauthor = myView.findViewById(R.id.tvauth);
            tvauthor.setText(s.getAuthor());

            ImageView imageView = myView.findViewById(R.id.ivimage);
            imageView.setVisibility(View.VISIBLE);
            File imgFile = new File(Environment.getExternalStorageDirectory().
                    getAbsolutePath() + "/komik_id/img/" + s.getJdl_kmk() + " " +
                    s.getKet() + ".jpg");
            if(imgFile.isFile()){
                Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                imageView.setImageBitmap(bitmap);
            } else {
                imageView.setVisibility(View.GONE);
            }

            final ListView listView = findViewById(R.id.lvEpsTampil);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String idlist = listnewsData.get(position).getId();

                    Intent intent = new Intent(getApplicationContext(), LihatEpisode.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("idkomik", idlist);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    LihatKomik.this.finish();
                }
            });

            return myView;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        listnewsData.clear();
        String like = " like ? or ";
        String selection = DBHelper.jdl_kmk + like + DBHelper.ket + " like ?";
        String args = "%" + newText + "%";
        String[] selectionargs = {args,args};
        String sortorder = DBHelper.id;
        Cursor cursor = dbHelper.Query(null, selection, selectionargs, sortorder);
        if (cursor.moveToFirst()) {
            do {
                listnewsData.add(new AdapterItems(
                        cursor.getString(cursor.getColumnIndex(DBHelper.id)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.jdl_kmk)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.ket)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.genre)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.publish)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.author))
                ));
            } while (cursor.moveToNext());
        }

        myadapter = new MyCustomAdapter(listnewsData);
        ListView lsNews = findViewById(R.id.lvEpsTampil);
        lsNews.setAdapter(myadapter);

        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem menuItem = menu.findItem(R.id.search);
        searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint("Cari...");
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_about:
                startActivity(new Intent(getApplicationContext(), Tentang.class));
                LihatKomik.this.finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void snackbarHapus(){
        b = false;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            b = bundle.getBoolean("hapus");
            if (b) {
                Snackbar.make(findViewById(R.id.eps_layout), "Data Berhasil Dihapus",
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }

    }

}