package com.example.komikid.Episode;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.example.komikid.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Objects;

import pub.devrel.easypermissions.EasyPermissions;

public class UpdateData extends AppCompatActivity {

    public static final int RESULT_LOAD_IMAGE = 1;
    public static final int TAKE_PICTURE = 2;

    ImageView imageView;
    EditText edjdul, edket, edgenre;
    Switch publish;
    TextView statusPub;

    String id;
    File fileImage;
    boolean gambarada = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_data);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        imageView = findViewById(R.id.imguview);
        edjdul = findViewById(R.id.eduJudul);
        edket = findViewById(R.id.eduKet);
        edgenre = findViewById(R.id.eduGenre);
        publish = findViewById(R.id.updtswitchpub);
        statusPub = findViewById(R.id.updtpublish);

        loadData();

        publish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    statusPub.setText("1");
                }else{
                    statusPub.setText("0");
                }
            }
        });

        Button bpfg = findViewById(R.id.bupicturefromgallery);
        bpfg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE};
                if (EasyPermissions.hasPermissions(getApplicationContext(), galleryPermissions)) {
                    pickImageFromGallery();
                } else {
                    EasyPermissions.requestPermissions(UpdateData.this, "Access for storage",
                            101, galleryPermissions);
                    pickImageFromGallery();
                }
            }
        });
    }

    private void loadData() {
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        id = bundle.getString("id_komik");
        String judul = bundle.getString("judul_komik");
        edjdul.setText(judul);
        String ket = bundle.getString("keterangan");
        edket.setText(ket);
        edgenre.setText(bundle.getString("genre"));
        String sPublish = bundle.getString("publish");

        if (sPublish != null && sPublish.equals("1")) {
            publish.setChecked(true);
        }
        else
            publish.setChecked(false);

        fileImage = new File(Environment.getExternalStorageDirectory().
                getAbsolutePath() + "/komik_id/img/" + judul + " " + ket+ ".jpg");
        if(fileImage.isFile()){
            Bitmap bitmap = BitmapFactory.decodeFile(fileImage.getAbsolutePath());
            imageView.setImageBitmap(bitmap);
            gambarada = true;
        } else {
            gambarada = false;
        }
    }

    void pickImageFromGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filepathcolumn = { MediaStore.Images.Media.DATA };

            assert selectedImage != null;
            Cursor cursor = getContentResolver().query(selectedImage,
                    filepathcolumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filepathcolumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = new BitmapDrawable(getApplicationContext().getResources(),
                    BitmapFactory.decodeFile(picturePath)).getBitmap();
            int nh = (int) ( bitmap.getHeight() * (1024.0 / bitmap.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 1024, nh, true);
            imageView.setImageBitmap(scaled);
        }

        if(requestCode == TAKE_PICTURE && resultCode == RESULT_OK) {
            //Bitmap bitmap1 = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
            File sdCard = Environment.getExternalStorageDirectory();
            File gambartemp = new File(sdCard.getAbsolutePath() + "/DCIM/temp.jpg");
            Bitmap bitmap1 = BitmapFactory.decodeFile(gambartemp.getAbsolutePath());

            Bitmap bitmap = new BitmapDrawable(getApplicationContext().getResources(),
                    bitmap1).getBitmap();
            int nh = (int) ( bitmap.getHeight() * (1024.0 / bitmap.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 1024, nh, true);
            imageView.setImageBitmap(scaled);
        }
    }

    public void updateData(View view) {

        DBHelper dbHelper = new DBHelper(this);
        ContentValues values = new ContentValues();

        if(TextUtils.isEmpty(edjdul.getText().toString().trim())) {
            if(TextUtils.isEmpty(edket.getText().toString().trim())) {
                Snackbar.make(view, "Judul dan Keterangan harus diisi", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
            else{
                Snackbar.make(view, "Judul harus diisi", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        } else if(TextUtils.isEmpty(edket.getText().toString().trim())){
            Snackbar.make(view, "Keterangan harus diisi", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else {

            // TODO: 02/02/2019 s
            if(gambarada){
                boolean b = fileImage.delete();
                if(!b){
                    return;
                }
            }
            values.put(DBHelper.id, id);
            values.put(DBHelper.jdl_kmk, edjdul.getText().toString());
            values.put(DBHelper.ket, edket.getText().toString());
            values.put(DBHelper.genre, edgenre.getText().toString());
            values.put(DBHelper.publish, statusPub.getText().toString());
            String selection = "id=?";
            String[] selectionArgs = {id};
            dbHelper.Update(values, selection, selectionArgs);

            Snackbar.make(view, "Berhasil Update Data", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE};

            if (EasyPermissions.hasPermissions(getApplicationContext(), galleryPermissions)) {
                BitmapDrawable draw = (BitmapDrawable) imageView.getDrawable();

                if(draw != null) {
                    Bitmap bitmap = draw.getBitmap();
                    FileOutputStream outStream = null;
                    File sdCard = Environment.getExternalStorageDirectory();
                    File dir = new File(sdCard.getAbsolutePath() + "/komik_id/img");
                    boolean isDirectoryCreated = dir.exists();
                    if (!isDirectoryCreated) {
                        isDirectoryCreated = dir.mkdirs();
                    }
                    if(isDirectoryCreated) {
                        String sjudul = edjdul.getText().toString();
                        String sket = edket.getText().toString();
                        String fileName = String.format(Locale.getDefault(),
                                sjudul + " " + sket + ".jpg", System.currentTimeMillis());
                        File outFile = new File(dir, fileName);
                        try {
                            outStream = new FileOutputStream(outFile);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                        try {
                            Objects.requireNonNull(outStream).flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            Objects.requireNonNull(outStream).close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

            } else {
                EasyPermissions.requestPermissions((Activity) getApplicationContext(), "Access for storage",
                        101, galleryPermissions);
            }
        }

    }

    public void clearData(View view) {
        imageView.setImageDrawable(null);
        edjdul.setText(null);
        edket.setText(null);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), TampilData.class);
        Bundle bundle = new Bundle();
        bundle.putString("idkomik", id);
        intent.putExtras(bundle);
        startActivity(intent);
        UpdateData.this.finish();
    }
}
