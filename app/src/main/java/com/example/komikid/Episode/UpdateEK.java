package com.example.komikid.Episode;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.komikid.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Objects;

import pub.devrel.easypermissions.EasyPermissions;

public class UpdateEK extends AppCompatActivity {

    public static final int RESULT_LOAD_IMAGE = 1;
    public static final int TAKE_PICTURE = 2;

    ImageView imageView;
    EditText edjdul;
    EditText edket;

    String id, id_komik, id_episode;
    File fileImage;
    boolean gambarada = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_ek);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        imageView = findViewById(R.id.imguviewEK);
        edjdul = findViewById(R.id.eduJudulEK);
        edket = findViewById(R.id.eduKetEK);

        loadData();

        Button bpfg = findViewById(R.id.bupicturefromgalleryEK);
        bpfg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE};
                if (EasyPermissions.hasPermissions(getApplicationContext(), galleryPermissions)) {
                    pickImageFromGallery();
                } else {
                    EasyPermissions.requestPermissions(UpdateEK.this, "Access for storage",
                            101, galleryPermissions);
                    pickImageFromGallery();
                }
            }
        });
    }

    private void loadData() {
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        id = bundle.getString("id_ek");
        id_komik = bundle.getString("id_komik");
        id_episode = bundle.getString("id_episode");
        Log.w("katon", "sebelum id_komik : " + id_komik + " id_episode : " + id_episode);
        String judul = bundle.getString("judul_EpsKmk");
        edjdul.setText(judul);
        String ket = bundle.getString("keterangan_EpsKmk");
        edket.setText(ket);

        fileImage = new File(Environment.getExternalStorageDirectory().
                getAbsolutePath() + "/komik_id/img/" + judul + " " + ket+ ".jpg");
        if(fileImage.isFile()){
            Bitmap bitmap = BitmapFactory.decodeFile(fileImage.getAbsolutePath());
            imageView.setImageBitmap(bitmap);
            gambarada = true;
        } else {
            gambarada = false;
        }
    }

    void pickImageFromGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filepathcolumn = { MediaStore.Images.Media.DATA };

            assert selectedImage != null;
            Cursor cursor = getContentResolver().query(selectedImage,
                    filepathcolumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filepathcolumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = new BitmapDrawable(getApplicationContext().getResources(),
                    BitmapFactory.decodeFile(picturePath)).getBitmap();
            int nh = (int) ( bitmap.getHeight() * (1024.0 / bitmap.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 1024, nh, true);
            imageView.setImageBitmap(scaled);
        }

        if(requestCode == TAKE_PICTURE && resultCode == RESULT_OK) {
            //Bitmap bitmap1 = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
            File sdCard = Environment.getExternalStorageDirectory();
            File gambartemp = new File(sdCard.getAbsolutePath() + "/DCIM/temp.jpg");
            Bitmap bitmap1 = BitmapFactory.decodeFile(gambartemp.getAbsolutePath());

            Bitmap bitmap = new BitmapDrawable(getApplicationContext().getResources(),
                    bitmap1).getBitmap();
            int nh = (int) ( bitmap.getHeight() * (1024.0 / bitmap.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 1024, nh, true);
            imageView.setImageBitmap(scaled);
        }
    }

    public void updateEK(View view) {

        DBHelper dbHelper = new DBHelper(this);
        ContentValues values = new ContentValues();

        if(TextUtils.isEmpty(edjdul.getText().toString().trim())) {
            if(TextUtils.isEmpty(edket.getText().toString().trim())) {
                Snackbar.make(view, "Judul dan Keterangan harus diisi", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
            else{
                Snackbar.make(view, "Judul harus diisi", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        } else if(TextUtils.isEmpty(edket.getText().toString().trim())){
            Snackbar.make(view, "Keterangan harus diisi", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else {

            // TODO: 02/02/2019 s
            if(gambarada){
                boolean b = fileImage.delete();
                if(!b){
                    return;
                }
            }
            values.put(DBHelper.id_ek, id);
            values.put(DBHelper.jdl_ek, edjdul.getText().toString());
            values.put(DBHelper.ket_ek, edket.getText().toString());
            String selection = "id_ek=?";
            String[] selectionArgs = {id};
            dbHelper.UpdateEK(values, selection, selectionArgs);

            Snackbar.make(view, "Berhasil Update Data", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE};

            if (EasyPermissions.hasPermissions(getApplicationContext(), galleryPermissions)) {
                BitmapDrawable draw = (BitmapDrawable) imageView.getDrawable();

                if(draw != null) {
                    Bitmap bitmap = draw.getBitmap();
                    FileOutputStream outStream = null;
                    File sdCard = Environment.getExternalStorageDirectory();
                    File dir = new File(sdCard.getAbsolutePath() + "/komik_id/img");
                    boolean isDirectoryCreated = dir.exists();
                    if (!isDirectoryCreated) {
                        isDirectoryCreated = dir.mkdirs();
                    }
                    if(isDirectoryCreated) {
                        String sjudul = edjdul.getText().toString();
                        String sket = edket.getText().toString();
                        String fileName = String.format(Locale.getDefault(),
                                sjudul + " " + sket + ".jpg", System.currentTimeMillis());
                        File outFile = new File(dir, fileName);
                        try {
                            outStream = new FileOutputStream(outFile);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                        try {
                            Objects.requireNonNull(outStream).flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            Objects.requireNonNull(outStream).close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

            } else {
                EasyPermissions.requestPermissions((Activity) getApplicationContext(), "Access for storage",
                        101, galleryPermissions);
            }
        }

    }

    public void clearData(View view) {
        imageView.setImageDrawable(null);
        edjdul.setText(null);
        edket.setText(null);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), LihatEpisodeKomik.class);
        Bundle bundle = new Bundle();
        bundle.putString("idlist", id);
        bundle.putString("id_komik", id_komik);
        bundle.putString("id_episode", id_episode);
        Log.w("katon", "sesudah id_komik : " + id_komik + " id_episode : " + id_episode);
        intent.putExtras(bundle);
        startActivity(intent);
        UpdateEK.this.finish();
    }
}
