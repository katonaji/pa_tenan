package com.example.komikid.Episode;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.komikid.Komik.AdapterKomik;
import com.example.komikid.MenuButton;
import com.example.komikid.R;

import java.io.File;
import java.util.ArrayList;

import pub.devrel.easypermissions.EasyPermissions;

public class TampilEK extends AppCompatActivity {

    DBHelper dbHelper;
    ImageView imageView;
    String id;
    String id_komik;
    boolean b;
    File fileImage;

    TextView tvtJdlKomik, tvKetKomik;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampil_ek);

        tvtJdlKomik = findViewById(R.id.tvJdlEpisodeKomik);
        tvKetKomik = findViewById(R.id.tvKetEpisodeKomik);

        dbHelper =  new DBHelper(this);
        imageView = findViewById(R.id.ivtampilgambarek);
        fileImage = tampilData();

        imageView.setClickable(true);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE};
                if (EasyPermissions.hasPermissions(getApplicationContext(), galleryPermissions)) {
                    openImage();
                } else {
                    EasyPermissions.requestPermissions(TampilEK.this,
                            "Access for storage", 101, galleryPermissions);
                    openImage();
                }
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab_ek);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(getApplicationContext(), TambahEK.class);

                Bundle bundle = new Bundle();
                bundle.putString("id_komik", id_komik);
                bundle.putString("id_episode", id);
                intent.putExtras(bundle);

                startActivity(intent);
                TampilEK.this.finish();
            }
        });

        dbHelper = new DBHelper(this);
        loadTable2();
    }

    private void openImage() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri imageUri = FileProvider.getUriForFile(getApplicationContext(),
                "com.example.komikid.provider", fileImage);
        intent.setDataAndType(imageUri, "image/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
    }

    private File tampilData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getString("id_episode");
            id_komik = bundle.getString("id_komik");
        }

        String[] projection = {"id_eps", "judul_eps", "keterangan_eps"};
        String selection = "id_eps=?";
        String[] selectionargs = {id};
        Cursor cursor = dbHelper.QueryEps(projection, selection, selectionargs, null);
        cursor.moveToFirst();

        String sjudul = cursor.getString(cursor.getColumnIndex(projection[1]));
        tvtJdlKomik.setText(sjudul);
        String sket = cursor.getString(cursor.getColumnIndex(projection[2]));
        tvKetKomik.setText(sket);

        File imgFile = new File(Environment.getExternalStorageDirectory().
                getAbsolutePath() + "/komik_id/img/" + sjudul + " " + sket + ".jpg");
        if(imgFile.isFile()){
            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            imageView.setImageBitmap(bitmap);
        }
        return imgFile;
    }

    public void hapusDataEK(View view) {
        new AlertDialog.Builder(this)
                .setMessage("apakah Anda Yakin?")
                .setCancelable(true)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String selection = "id_eps=?";
                        String[] selectionArgs = {id};
                        int temp;
                        temp = dbHelper.DeleteEps(selection, selectionArgs);
                        if(temp != 0) {
                            if(fileImage.isFile()){
                                boolean b = fileImage.delete();
                                if(!b){
                                    return;
                                }
                            }
                            Intent intent = new Intent(getApplicationContext(),TampilData.class);
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("hapus", true);
                            bundle.putString("idlist", id);
                            bundle.putString("idkomik", id_komik);

                            intent.putExtras(bundle);
                            startActivity(intent);
                            TampilEK.this.finish();
                        }
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), TampilData.class);
        Bundle bundle = new Bundle();
        bundle.putString("idlist", id);
        bundle.putString("idkomik", id_komik);

        intent.putExtras(bundle);
        startActivity(intent);
        TampilEK.this.finish();
    }

    ArrayList<AdapterEK> listnewsEK= new ArrayList<>();
    MyCustomAdapter myadapter;

    private void loadTable2() {
        listnewsEK.clear();
        String[] projection1 = {DBHelper.id_ek, DBHelper.jdl_ek, DBHelper.ket_ek};
        String selection = "id_ekomik=?";
        String[] selectionargs = {id};
        String sortorder1 = DBHelper.id_ek;
        Cursor cursor = dbHelper.QueryEK(projection1, selection, selectionargs, sortorder1);
        if (cursor.moveToFirst()) {
            do {
                listnewsEK.add(new AdapterEK(cursor.getString(cursor.getColumnIndex(DBHelper.id_ek)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.jdl_ek)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.ket_ek))));
            } while (cursor.moveToNext());
        }

        myadapter = new MyCustomAdapter(listnewsEK);
        ListView lsNews1 = findViewById(R.id.lvtampil);
        lsNews1.setAdapter(myadapter);
        TextView tvKosong = findViewById(R.id.tvkosong);

        if(myadapter.isEmpty()) {
            lsNews1.setVisibility(View.GONE);
            tvKosong.setVisibility(View.VISIBLE);
        } else {
            lsNews1.setVisibility(View.VISIBLE);
            tvKosong.setVisibility(View.GONE);
        }
    }

    private class MyCustomAdapter extends BaseAdapter {
        ArrayList<AdapterEK> listnewsEKAdpater ;

        MyCustomAdapter(ArrayList<AdapterEK> listnewsEKAdpater) {
            this.listnewsEKAdpater=listnewsEKAdpater;
        }

        @Override
        public int getCount() {
            return listnewsEKAdpater.size();
        }

        @Override
        public String getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater mInflater1 = getLayoutInflater();
            View myView = mInflater1.inflate(R.layout.layout_ek,null);
            final AdapterEK s = listnewsEKAdpater.get(position);

            ImageView imageView = myView.findViewById(R.id.ivLEK);
            imageView.setVisibility(View.VISIBLE);
            File imgFile = new File(Environment.getExternalStorageDirectory().
                    getAbsolutePath() + "/komik_id/img/" + s.getJdl_ek() + " " +
                    s.getKet_ek() + ".jpg");
            if(imgFile.isFile()){
                Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                imageView.setImageBitmap(bitmap);
            } else {
                imageView.setVisibility(View.GONE);
            }

            final ListView listView1 = findViewById(R.id.lvtampil);
            listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id1) {
                    String idlist1 = listnewsEKAdpater.get(position).getId_ek();

                    Intent intent1 = new Intent(getApplicationContext(), LihatEpisodeKomik.class);
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("idlist", idlist1);
                    bundle1.putString("id_komik", id_komik);
                    bundle1.putString("id_episode", id);
                    Log.w("katon", "kirim" + id);
                    intent1.putExtras(bundle1);
                    startActivity(intent1);
                    TampilEK.this.finish();
                }
            });

            return myView;
        }
    }
    public void updateEpisode(View view) {
        Intent intent = new Intent(getApplicationContext(), UpdateEpisode.class);
        Bundle bundle = new Bundle();
        bundle.putString("id_eps", id);
        bundle.putString("id_komik", id_komik);
        bundle.putString("judul_eps", tvtJdlKomik.getText().toString());
        bundle.putString("keterangan_eps", tvKetKomik.getText().toString());

        intent.putExtras(bundle);
        startActivity(intent);
        TampilEK.this.finish();
    }
}
