package com.example.komikid.Episode;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.komikid.Komik.AdapterKomik;
import com.example.komikid.R;

import java.io.File;
import java.util.ArrayList;

import pub.devrel.easypermissions.EasyPermissions;

public class LihatEpisode extends AppCompatActivity {

    DBHelper dbHelper;
    ImageView imageView;
    String id;
    boolean b;
    File fileImage;

    TextView tvtJdlKomik, tvKetKomik;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_episode);

        tvtJdlKomik = findViewById(R.id.tvJdlKomik);
        tvKetKomik = findViewById(R.id.tvKetKomik);

        dbHelper =  new DBHelper(this);
        imageView = findViewById(R.id.ivtampilgambar);
        fileImage = tampilData();

        imageView.setClickable(true);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE};
                if (EasyPermissions.hasPermissions(getApplicationContext(), galleryPermissions)) {
                    openImage();
                } else {
                    EasyPermissions.requestPermissions(LihatEpisode.this,
                            "Access for storage", 101, galleryPermissions);
                    openImage();
                }
            }
        });

        dbHelper = new DBHelper(this);
        loadTable1();
    }

    private void openImage() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri imageUri = FileProvider.getUriForFile(getApplicationContext(),
                "com.example.komikid.provider", fileImage);
        intent.setDataAndType(imageUri, "image/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
    }

    private File tampilData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getString("idkomik");
        }

        String[] projection = {"id", "judul_komik", "keterangan"};
        String selection = "id=?";
        String[] selectionargs = {id};
        Cursor cursor = dbHelper.Query(projection, selection, selectionargs, null);
        cursor.moveToFirst();
        String sjudul = cursor.getString(cursor.getColumnIndex(projection[1]));
        tvtJdlKomik.setText(sjudul);
        String sket = cursor.getString(cursor.getColumnIndex(projection[2]));
        tvKetKomik.setText(sket);

        File imgFile = new File(Environment.getExternalStorageDirectory().
                getAbsolutePath() + "/komik_id/img/" + sjudul + " " + sket + ".jpg");
        if(imgFile.isFile()){
            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            imageView.setImageBitmap(bitmap);
        }
        return imgFile;
    }

    public void hapusData(View view) {
        new AlertDialog.Builder(this)
                .setMessage("apakah Anda Yakin?")
                .setCancelable(true)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String selection = "id=?";
                        String[] selectionArgs = {id};
                        int temp;
                        temp = dbHelper.Delete(selection, selectionArgs);
                        if(temp != 0) {
                            if(fileImage.isFile()){
                                boolean b = fileImage.delete();
                                if(!b){
                                    return;
                                }
                            }
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("hapus", true);
                            startActivity(new Intent(getApplicationContext(),
                                    MainEpisode.class).putExtras(bundle));
                            LihatEpisode.this.finish();
                        }
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), LihatKomik.class));
        LihatEpisode.this.finish();
    }

    ArrayList<AdapterKomik> listnewsEpisode = new ArrayList<>();
    MyCustomAdapter myadapter;

    private void loadTable1() {
        listnewsEpisode.clear();
        String[] projection1 = {DBHelper.id_eps, DBHelper.jdl_eps, DBHelper.ket_eps, DBHelper.baca};
        String selection = "id_episode=?";
        String[] selectionargs = {id};
        String sortorder1 = DBHelper.id_eps + " DESC";
        Cursor cursor = dbHelper.QueryEps(projection1, selection, selectionargs, sortorder1);
        if (cursor.moveToFirst()) {
            do {
                listnewsEpisode.add(new AdapterKomik(cursor.getString(cursor.getColumnIndex(DBHelper.id_eps)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.jdl_eps)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.ket_eps)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.baca))
                ));
            } while (cursor.moveToNext());
        }

        myadapter = new MyCustomAdapter(listnewsEpisode);
        ListView lsNews1 = findViewById(R.id.lvEpisode);
        lsNews1.setAdapter(myadapter);
        TextView tvKosong = findViewById(R.id.tvKmkKosong);

        if(myadapter.isEmpty()) {
            lsNews1.setVisibility(View.GONE);
            tvKosong.setVisibility(View.VISIBLE);
        } else {
            lsNews1.setVisibility(View.VISIBLE);
            tvKosong.setVisibility(View.GONE);
        }
    }

    private class MyCustomAdapter extends BaseAdapter {
        ArrayList<AdapterKomik> listnewsKomikAdpater ;

        MyCustomAdapter(ArrayList<AdapterKomik> listnewsKomikAdpater) {
            this.listnewsKomikAdpater=listnewsKomikAdpater;
        }

        @Override
        public int getCount() {
            return listnewsKomikAdpater.size();
        }

        @Override
        public String getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater mInflater1 = getLayoutInflater();
            View myView = mInflater1.inflate(R.layout.layout_episode,null);
            final AdapterKomik s = listnewsKomikAdpater.get(position);

            final TextView tvJdlKmk = myView.findViewById(R.id.tvEpisode);
            tvJdlKmk.setText(s.getJdl_eps());

            ImageView imageView = myView.findViewById(R.id.ivLepisode);
            imageView.setVisibility(View.VISIBLE);
            File imgFile = new File(Environment.getExternalStorageDirectory().
                    getAbsolutePath() + "/komik_id/img/" + s.getJdl_eps() + " " +
                    s.getKet_eps() + ".jpg");
            if(imgFile.isFile()){
                Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                imageView.setImageBitmap(bitmap);
            } else {
                imageView.setVisibility(View.GONE);
            }

            final ListView listView1 = findViewById(R.id.lvEpisode);
            listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id1) {
                    String id_episode = listnewsKomikAdpater.get(position).getId_eps();
                    Log.i("KATON", "Position : " + position + " Id Episode : " + id_episode + " Baca : " + (Integer.parseInt(listnewsKomikAdpater.get(position).getBaca()) + 1));

//                    ContentValues values = new ContentValues();
//                    values.put(DBHelper.baca, Integer.parseInt(listnewsKomikAdpater.get(position).getBaca())+1);
//                    String selection = "id_episode=?";
//                    String[] selectionArgs = {id_episode};
//                    dbHelper.UpdateEK(values, selection, selectionArgs);

                    dbHelper.ExecSql("UPDATE Komik SET baca = baca+1 where id_eps = " + id_episode);

                    Log.i("KATON", "Position : " + position + " Id Episode : " + id_episode + " Baca : " + (Integer.parseInt(listnewsKomikAdpater.get(position).getBaca()) + 1));

                    Intent intent1 = new Intent(getApplicationContext(), LihatEK.class);
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("id_episode", id_episode);
                    bundle1.putString("id_komik", id);
                    intent1.putExtras(bundle1);
                    startActivity(intent1);
                    LihatEpisode.this.finish();
                }
            });

            return myView;
        }
    }
}