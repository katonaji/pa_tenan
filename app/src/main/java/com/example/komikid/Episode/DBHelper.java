package com.example.komikid.Episode;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

public class DBHelper {
    private SQLiteDatabase sqlDB;
    private static final int DBVersion = 1;
    private static final String DBName = "komik_id";
    private static final String TName = "Episode";
    static final String id = "id";
    static final String ket = "keterangan";
    static final String genre = "genre";
    static final String jdl_kmk = "judul_komik";
    static final String publish = "publish";
    static final String author = "author";

    private static final String TName1 = "Komik";
    static final String id_eps = "id_eps";
    static final String ket_eps = "keterangan_eps";
    static final String jdl_eps = "judul_eps";
    static final String baca = "baca";
    static final String id_episode = "id_episode";

    private static final String TName2 = "Episode_komik";
    static final String id_ek = "id_ek";
    static final String jdl_ek = "judul_EpsKmk";
    static final String ket_ek = "keterangan_EpsKmk";
    static final String id_ekomik = "id_ekomik";

    private static final String BuatEpisode = "Create table IF NOT EXISTS " + TName +
            "(id integer PRIMARY KEY AUTOINCREMENT,"+
            jdl_kmk + " text," + ket + " text," + genre + " text," + publish + " text," + author + " text);";

    private static final String BuatKomik = "Create table IF NOT EXISTS " + TName1 +
            "(id_eps integer PRIMARY KEY AUTOINCREMENT,"+ jdl_eps +
            " text," + id_episode + " integer," + ket_eps + " text," + baca + " integer);";

    private static final String BuatEpsKmk = "Create table IF NOT EXISTS " + TName2 +
            "(id_ek integer PRIMARY KEY AUTOINCREMENT,"+ jdl_ek +
            " text," + id_ekomik + " integer," + ket_ek + " text);";

    static class DatabaseHelperUser extends SQLiteOpenHelper {
        Context context;
        DatabaseHelperUser(Context context) {
            super(context,DBName,null,DBVersion);
            this.context=context;
        }

        @Override
        public void onCreate(SQLiteDatabase db){
            db.execSQL(BuatKomik);
            db.execSQL(BuatEpisode);
            db.execSQL(BuatEpsKmk);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("Drop table IF EXISTS "+ TName);
            db.execSQL("Drop table IF EXISTS "+ TName1);
            db.execSQL("Drop table IF EXISTS "+ TName2);
            onCreate(db);
        }
    }

    DBHelper(Context context) {
        DatabaseHelperUser db = new DatabaseHelperUser(context);
        sqlDB=db.getWritableDatabase();
    }

    public void InsertEps(ContentValues values1) {
        sqlDB.insert(TName1, "", values1);
    }
    public Cursor QueryEps(String[] Projection1, String Selection1, String[] SelectionArgs1, String SortOrder1) {
        SQLiteQueryBuilder queryBuilder1 = new SQLiteQueryBuilder();
        queryBuilder1.setTables(TName1);
        return queryBuilder1.query(sqlDB, Projection1, Selection1, SelectionArgs1,
                null, null, SortOrder1);
    }
    public int DeleteEps(String selection, String[] selectionargs){
        return sqlDB.delete(TName1, selection, selectionargs);
    }
    public int UpdateEps(ContentValues contentValues1, String selection1, String[] selectionArgs1) {
        return sqlDB.update(TName1, contentValues1, selection1, selectionArgs1);
    }

    //Insert,Query,Delete,Update Komik
    public void Insert(ContentValues values) {
        sqlDB.insert(TName, "", values);
    }
    public Cursor Query(String[] Projection, String Selection, String[] SelectionArgs, String SortOrder){
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TName);
        return queryBuilder.query(sqlDB, Projection, Selection, SelectionArgs,
                null, null, SortOrder);
    }
    public int Delete(String selection, String[] selectionargs){
        return sqlDB.delete(TName, selection, selectionargs);
    }
    public int Update(ContentValues contentValues, String selection, String[] selectionArgs) {
        return sqlDB.update(TName, contentValues, selection, selectionArgs);
    }

    //Insert,Query,Delete,Update EpsKmk
    public void InsertEK(ContentValues values2) {
        sqlDB.insert(TName2, "", values2);
    }
    public Cursor QueryEK(String[] Projection2, String Selection2, String[] SelectionArgs2, String SortOrder2){
        SQLiteQueryBuilder queryBuilder2 = new SQLiteQueryBuilder();
        queryBuilder2.setTables(TName2);
        return queryBuilder2.query(sqlDB, Projection2, Selection2, SelectionArgs2,
                null, null, SortOrder2);
    }
    public int DeleteEK(String selection2, String[] selectionargs2){
        return sqlDB.delete(TName2, selection2, selectionargs2);
    }
    public int UpdateEK(ContentValues contentValues2, String selection2, String[] selectionArgs2){
        return sqlDB.update(TName1, contentValues2, selection2, selectionArgs2);
    }

    public void ExecSql(String sql){
        sqlDB.execSQL(sql);
    }
}