package com.example.komikid.Episode;

public class AdapterItems {
    private String id;
    private String ket;
    private String jdl_kmk;
    private String genre;
    private String publish;
    private String author;

    AdapterItems(String id, String jdl_kmk, String ket, String genre, String publish, String author){
        this.id = id;
        this.jdl_kmk = jdl_kmk;
        this.ket = ket;
        this.genre = genre;
        this.publish = publish;
        this.author = author;
    }

    public String getId() {
        return id;
    }

    public String getKet() {
        return ket;
    }

    public String getJdl_kmk() {
        return jdl_kmk;
    }

    public String getGenre() {
        return genre;
    }

    public String getPublish() {
        return publish;
    }

    public String getAuthor() {
        return author;
    }
}
