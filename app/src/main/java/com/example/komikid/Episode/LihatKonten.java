package com.example.komikid.Episode;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.komikid.R;

import java.io.File;

import pub.devrel.easypermissions.EasyPermissions;

public class LihatKonten extends AppCompatActivity {

    DBHelper dbHelper;
    ImageView imageView;
    String id, id_komik, id_episode;
    boolean b;
    File fileImage;

    TextView tvtJdlKomik, tvKetKomik;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_konten);

        tvtJdlKomik = findViewById(R.id.tvLihatJdlek);
        tvKetKomik = findViewById(R.id.tvLihatKetek);

        dbHelper =  new DBHelper(this);
        imageView = findViewById(R.id.ivlihatek);
        fileImage = tampilData();

        imageView.setClickable(true);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE};
                if (EasyPermissions.hasPermissions(getApplicationContext(), galleryPermissions)) {
                    openImage();
                } else {
                    EasyPermissions.requestPermissions(LihatKonten.this,
                            "Access for storage", 101, galleryPermissions);
                    openImage();
                }
            }
        });

        dbHelper = new DBHelper(this);
        //loadTable1();
    }

    private void openImage() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri imageUri = FileProvider.getUriForFile(getApplicationContext(),
                "com.example.komikid.provider", fileImage);
        intent.setDataAndType(imageUri, "image/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
    }

    private File tampilData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getString("idlist");
            id_komik = bundle.getString("id_komik");
            id_episode = bundle.getString("id_episode");
            Log.w("katon", "sesudah " + id_episode);
        }

        String[] projection5 = {"id_ek", "judul_EpsKmk", "keterangan_EpsKmk"};
        String selection5 = "id_ek=?";
        String[] selectionargs5 = {id};
        Cursor cursor = dbHelper.QueryEK(projection5, selection5,selectionargs5, null);
        cursor.moveToFirst();

        String sjudul = cursor.getString(cursor.getColumnIndex(projection5[1]));
//        tvtJdlKomik.setText(sjudul);
        String sket = cursor.getString(cursor.getColumnIndex(projection5[2]));
//        tvKetKomik.setText(sket);

        File imgFile = new File(Environment.getExternalStorageDirectory().
                getAbsolutePath() + "/komik_id/img/" + sjudul + " " + sket + ".jpg");
        if(imgFile.isFile()){
            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            imageView.setImageBitmap(bitmap);
        }
        return imgFile;
    }

    public void hapusLihatEK(View view) {
        new AlertDialog.Builder(this)
                .setMessage("apakah Anda Yakin?")
                .setCancelable(true)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String selection = "id_ek=?";
                        String[] selectionArgs = {id};
                        int temp;
                        temp = dbHelper.Delete(selection, selectionArgs);
                        if(temp != 0) {
                            if(fileImage.isFile()){
                                boolean b = fileImage.delete();
                                if(!b){
                                    return;
                                }
                            }
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("hapus", true);
                            startActivity(new Intent(getApplicationContext(),
                                    MainEpisode.class).putExtras(bundle));
                            LihatKonten.this.finish();
                        }
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), LihatEK.class);
        Bundle bundle = new Bundle();
        bundle.putString("idlist", id);
        bundle.putString("id_komik", id_komik);
        bundle.putString("id_episode", id_episode);
        Log.w("katon", "aaab " + id_episode);

        intent.putExtras(bundle);
        startActivity(intent);
        LihatKonten.this.finish();
    }
}

