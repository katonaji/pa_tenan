package com.example.komikid;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.komikid.Episode.LihatKomik;
import com.example.komikid.Episode.MainEpisode;
import com.example.komikid.Episode.TambahEK;
import com.example.komikid.Episode.TampilEK;

public class MenuButton extends AppCompatActivity {

    Button listkmk, aba;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_button);

        Toolbar toolbar = findViewById(R.id.bar);
        setSupportActionBar(toolbar);

        aba = (Button) findViewById(R.id.aba);
        listkmk = (Button) findViewById(R.id.list_kmk);


        /*coba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuButton.this,Login.class);
                startActivity(intent);
            }
        });*/

        listkmk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v==listkmk){
                    Intent intent = new Intent(MenuButton.this, LihatKomik.class);
                    startActivity(intent);
                }
            }
        });

        aba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == aba){
                    Intent intent = new Intent(MenuButton.this, MainEpisode.class);
                    startActivity(intent);
                }
            }
        });
    }
}