package com.example.komikid;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.komikid.Singleton.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity{
    Button reg_bn;
    EditText Name,Email,Username,Password,Confirm;
    String name,email,username,password,confirm;
    TextView login;
    AlertDialog.Builder builder;
    //private static final String reg_url = "http://192.168.1.5/database/register.php";
    private static final String reg_url = "http://192.168.43.240/database/register.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        login = (TextView)findViewById(R.id.tmb_login);
        reg_bn = (Button)findViewById(R.id.btnRegiter);
        Name = (EditText)findViewById(R.id.reg_inptNama);
        Email = (EditText)findViewById(R.id.reg_inptEmail);
        Username = (EditText)findViewById(R.id.reg_inptUsername);
        Password = (EditText)findViewById(R.id.reg_inptPassword);
        Confirm = (EditText)findViewById(R.id.reg_inptConfirm);
        builder = new AlertDialog.Builder(Register.this);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Register.this, Login.class);
                startActivity(intent);
            }
        });
        reg_bn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = Name.getText().toString().trim();
                email = Email.getText().toString().trim();
                username = Username.getText().toString().trim();
                password = Password.getText().toString().trim();
                confirm = Confirm.getText().toString().trim();

                if(name.equals("")||email.equals("")||username.equals("")||password.equals("")||confirm.equals("")){
                    builder.setTitle("Someting want wrong....");
                    builder.setMessage("Please Input all Field....");
                    displayalert("Inpurt eror");
                }else{
                    if(!(password.equals(confirm))){
                        builder.setTitle("Someting want wrong....");
                        builder.setMessage("Password is not matching");
                        displayalert("Inpurt eror");
                    }else{
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, reg_url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            JSONArray jsonArray = new JSONArray(response);
                                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                                            String code = jsonObject.getString("code");
                                            String message = jsonObject.getString("message");
                                            builder.setTitle("server response....");
                                            builder.setMessage(message);
                                            Intent intent = new Intent(Register.this,Login.class);
                                            startActivity(intent);
                                            displayalert(code);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }){

                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError{
                                Map <String, String> params = new HashMap<String, String>();
                                params.put("nama",name);
                                params.put("email",email);
                                params.put("user_name",username);
                                params.put("password",password);
                                return params;
                            }
                        };
                        MySingleton.getInstance(Register.this).addToRequestqueue(stringRequest);
                    }
                }
                //Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);

            }
        });

    }

    public void displayalert(final String code){
        builder.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(code.equals("Input eror")){
                    Password.setText("");
                    Confirm.setText("");
                }else if(code.equals("req_sucsses")){
                    finish();
                }else if (code.equals("req_failed")){
                    Name.setText("");
                    Email.setText("");
                    Username.setText("");
                    Password.setText("");
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
