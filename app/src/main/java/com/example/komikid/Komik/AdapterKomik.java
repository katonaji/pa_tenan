package com.example.komikid.Komik;

public class AdapterKomik {
    private String id_eps;
    private String ket_eps;
    private String jdl_eps;
    private String baca;

    public AdapterKomik(String id_eps, String jdl_eps, String ket_eps, String baca) {
        this.id_eps = id_eps;
        this.jdl_eps = jdl_eps;
        this.ket_eps = ket_eps;
        this.baca = baca;
    }

    public String getId_eps() {
        return id_eps;
    }

    public String getKet_eps() {
        return ket_eps;
    }

    public String getJdl_eps() {
        return jdl_eps;
    }

    public String getBaca() {
        return baca;
    }
}
